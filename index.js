var exec = require('child_process').execSync;
var async = require('async');
var fs = require('fs');
var debug = require('debug')('Songer');
var csv = require('csv-string');
var formula = require( process.argv[2] );
var arg1 = process.argv[3];

var info = {}; //All song info

//Load effects / task libs
var identUtil = require('./lib/identUtil');
var taskName = require('./lib/name.js');
var taskDownload = require('./lib/download');
var taskGetKey = require('./lib/getKey');
var taskLength = require('./lib/lengthTime.js');
var effectStretch = require('./lib/stretch');
var effectPitch = require('./lib/pitch');

function handleEffect( effect, info, target,cb ) {
	switch (effect.type) {
		case 'stretch' : 
			effectStretch(info.ident,target,effect.multiple,effect.size);
			cb();
		break;
		case 'pitch' : 
			effectPitch(info.ident,target,effect.cents); 
			cb();
		break;
	}
}

function handleTrack( trackName, track, info, cb ) {
	if (track.source == 'base') {
		childCP = exec('cp table/'+info.ident+'/original.wav output/'+info.ident+'/'+trackName+'.wav');
	}
	async.everySeries( formula.base.effects , function( effect, icb ) {			
		return handleEffect( effect, info, trackName, icb);
	}, cb );
}

async.series([
	function(cb) { //Download
		if (formula.base.source == 'arg1') {
			info.ident = identUtil(arg1);
			debug('Downloading base file');
			info.originalURL = arg1;
			taskDownload(arg1,info.ident, function(err) {
				cb();	
			});	
		}
	} ,
	function (cb) { //Name it
		var nameParts = require('./names/'+formula.base.name);		
		//Seed a random
		var seed = function() {
			var x = Math.sin(parseInt(info.ident,16)) * 10000;
			return (x - Math.floor(x));
		}
		info.name = taskName(seed,nameParts.adjectives() ,nameParts.nouns() );
		debug('Named '+info.name);
		cb();
	},
	function (cb) { //Convert to Midi
		taskGetKey(info.ident,'original', function(err, reply) {
			info.key = reply;
			cb();
		});
	},
	//Base Effects
	function (cb) {
		var counter = 0;
		async.everySeries( formula.base.effects , function( effect, icb ) {			
			return handleEffect( effect,info,'original',icb);
		},cb );
	},
	function (cb) { //Build BPM from original
		taskLength(info.ident,'original',function(err,results) {
			info.lengthSeconds = results.lengthTime;
			info.BPM = results.bpm;
			cb();
		});
	},
	// Process all other tracks
	function (cb) {
		debug('Processing all tracks');
		async.each( formula, function( trackIndex, icb ) {
			if (trackIndex !== 'base') {
				handleTrack( trackIndex, formula[ trackIndex ], info, icb );
			} else {
				icb();
			}
		}, cb);
	},
	//Mixdown channel
	function (cb) {
		debug('Mixing down tracks');
		childCP = exec('cp table/'+info.ident+'/original.wav output/'+info.ident+'/mixdown.wav');
		async.eachSeries( formula , function( trackName , icb ) {
			childMX = exec('sox -m table/'+info.ident+'/'+trackName+'.wav output/'+info.ident+'/mixdown.wav');
			icb();
		},cb );
	},
	
	//Convert final to mp3
	function (cb) { //Convert to MP3
		debug('Converting to MP3');
		childLM = exec('lame table/'+info.ident+'/finished.wav output/'+info.ident+'.mp3');
		cb();
	},

	function (cb) { //Write JSON file
		debug('Writing info file');
		fs.writeFile('output/'+info.ident+'.json', JSON.stringify(info));
		cb();
	},
	function (cb) { //Clean up
		debug('Cleaning mix table');
		childRM = exec('rm -rf table/'+info.ident);
		cb();
	}
], function( results, cb) {
	debug('Completed');
	console.log(JSON.stringify(info));	
});

