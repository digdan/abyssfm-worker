#!/usr/bin/sh
# Delete temporary table directories older than 1 days
BASEDIR=$(dirname "$0")
echo "$BASEDIR/table/*";
find $BASEDIR/table/* -type d -ctime +1 -exec rm -rf {} \;
