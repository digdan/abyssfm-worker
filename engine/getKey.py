#!/usr/bin/env python
import music21
import sys
score = music21.converter.parse(sys.argv[1])
key = score.analyze('key')
print(key.tonic.name, key.mode, key.tonalCertainty())
print(key.alternateInterpretations[0].tonic.name,key.alternateInterpretations[0].mode, key.alternateInterpretations[0].correlationCoefficient )
print(key.alternateInterpretations[1].tonic.name,key.alternateInterpretations[1].mode, key.alternateInterpretations[1].correlationCoefficient )
print(key.alternateInterpretations[2].tonic.name,key.alternateInterpretations[2].mode, key.alternateInterpretations[2].correlationCoefficient )
