#!/usr/bin/php -q
<?php
	if (count($argv) == 1) {
			echo "Please include a youtube url to stretch.\n";
			echo "Ex: ".$argv[0]." https://www.youtube.com/watch?v=HwcG3mYo5Kk \n";
		die();
	}
	$target = $argv[1];
	$id = uniqid();
	$command = "/usr/bin/youtube-dl --audio-format wav --audio-quality 0 --extract-audio {$target} --user-agent ViscousBot -o temp/{$id}.%\(ext\)s";
	echo "Downloading source file.\n";
	exec($command);
/*
	$command = "sox temp/{$id}.wav output/{$id}.wav ladspa tap_deesser tap_deesser -30 6200";
	echo "Deessing\n";
	exec($command);
*/
	$command = "mv temp/{$id}.wav output/{$id}-1.wav";
	exec($command);

	$command = "rm -f temp/{$id}.*";
	echo "Removing temporary working files.\n";
	exec($command);

	//STRETCH 8
	$command = "python paul.py output/{$id}-1.wav -s 8 output/{$id}-8.wav";
	echo "Stretching file 8x.\n";
	exec($command);

	//STRETCH 4
	$command = "python paul.py output/{$id}-1.wav -s 4 output/{$id}-4.wav";
	echo "Stretching file 4x.\n";
	exec($command);

	//Append
	$command = "sox output/{$id}-4.wav output/{$id}-4.wav output/{$id}-4a4.wav";
	echo "Appending 4x's\n";
	exec($command);

	$command = "sox -m output/{$id}-8.wav output/{$id}-4a4.wav output/{$id}-combined.wav";
	echo "Combining wavs.\n";
	exec($command);	

	$command = "sox output/{$id}-combined.wav output/{$id}-final.wav silence 1 0.1 0.1% reverse silence 1 0.1 0.1% reverse";
	echo "Trimming silence.\n";
	exec($command);


	$command = "/usr/local/bin/lame output/{$id}-final.wav output/{$id}.mp3";
	echo "Converting file to MP3\n";
	exec($command);

	$command = "rm -f output/{$id}-*.wav";
	echo "Removing temporary working files.\n";
	exec($command);

	echo "Completed : output/{$id}.mp3\n";
?>
