#!/usr/bin/php -q
<?php
	if (count($argv) == 1) {
			echo "Please include a youtube url to stretch.\n";
			echo "Ex: ".$argv[0]." https://www.youtube.com/watch?v=HwcG3mYo5Kk \n";
		die();
	}
	$target = $argv[1];
	$id = uniqid();
	$command = "/usr/bin/youtube-dl --audio-format wav --audio-quality 0 --extract-audio {$target} --user-agent ViscousBot -o temp/{$id}.%\(ext\)s";
	echo "Downloading source file.\n";
	exec($command);
/*
	$command = "sox temp/{$id}.wav output/{$id}.wav ladspa tap_deesser tap_deesser -30 6200";
	echo "Deessing\n";
	exec($command);
*/
	$command = "soundstretch temp/{$id}.wav -bpm 2>&1";
	echo "Calculating BPM\n";
	$line = exec($command,$bpmData);
	$bpm = 0;
	foreach($bpmData as $bpmLine) {
		if (strstr($bpmLine,"Detected BPM rate")) {
			$bpm = substr($bpmLine,18);
		}
	}

	$halfHertz = round(($bpm / 60) / 4,4);
	$hertz = round(($bpm / 60) / 2,4);
	echo "BPM : {$bpm}\n";
	echo "Hertz : {$hertz}\n";

	$command = "mv temp/{$id}.wav output/{$id}-1.wav";
	exec($command);

	$command = "rm -f temp/{$id}.*";
	echo "Removing temporary working files.\n";
	exec($command);

	//STRETCH 8
	$command = "python paul.py output/{$id}-1.wav -s 8 output/{$id}-8.wav";
	echo "Stretching file 8x.\n";
	exec($command);

	$command = "sox output/{$id}-8.wav output/{$id}-effected.wav tremolo {$halfHertz} 40";
	echo "Tremolo Half Hertz\n";
	exec($command);

	$command = "sox output/{$id}-effected.wav output/{$id}-effectedFinal.wav tremolo {$hertz} 40 reverb";
	echo "Tremolo full Hertz\n";
	exec($command);

	$command = "sox output/{$id}-effectedFinal.wav output/{$id}-final.wav silence 1 0.1 0.1% reverse silence 1 0.1 0.1% reverse";
	echo "Trimming silence.\n";
	exec($command);


	$command = "/usr/local/bin/lame output/{$id}-final.wav output/{$id}.mp3";
	echo "Converting file to MP3\n";
	exec($command);

	$command = "rm -f output/{$id}-*.wav";
	echo "Removing temporary working files.\n";
	exec($command);

	echo "Completed : output/{$id}.mp3\n";
?>
