module.exports.nouns = function () { 
	return [
		'Crest',
		'Glimmer',
		'Octopus',
		'Whale',
		'Plankton',
		'Crab',
		'Wave',
		'Cosmos',
		'Wreckage',
		'Abyss',
		'Deep',
		'Depths'
	] 
};


module.exports.adjectives = function() {
	return [
		'Washed',
		'Aqua',
		'Beautiful',
		'Briny',
		'Blue',
		'Turquoise',
		'Calm',
		'Cerulean',
		'Deep',
		'Immense',
		'Marine',
		'Paradise',
		'Rippling',
		'Salty',
		'Shifty',
		'Wide',
		'Wonderous',
		'Wavy',
		'Tranquil',
		'Green',
		'Suspended',
		'Floating',
		'Boyant',
		'Diving',
		'Coasting',
		'Falling'
	];
}
