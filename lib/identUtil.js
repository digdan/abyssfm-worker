var md5 = require('md5');

module.exports = function(source) {
        var hash = md5(source);
        return hash.substr(0,6);
}

