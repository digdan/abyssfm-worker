function randomItem(generator,items){
    generator = generator || Math.random;
    return items[Math.floor(generator()*items.length)];
}

function generateName(generator,adjectives,nouns){
    var noun1 = randomItem(generator,nouns);
    var noun2 = randomItem(generator,nouns);
    noun2 = noun2.substr(0, 1).toUpperCase() + noun2.substr(1);
    var adjective = randomItem(generator,adjectives);
    return adjective + noun1 + ' ' + noun2;
}

module.exports = generateName;
