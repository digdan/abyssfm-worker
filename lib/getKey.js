var exec = require('child_process').execSync;
var fs = require('fs');
var debug = require('debug')('getKey');
var url = require('url');
var csv = require('csv-string');


function getKey(ident,target,cb) {
	debug('Trimming original to sample');
	 exec('sox table/'+ident+'/'+target+'.wav table/'+ident+'/'+target+'-sample.wav reverse trim 5 5 reverse'); //Trim to last 5 to 10 seconds
	debug('Converting WAV to MIDI');
	childWA = exec('./engine/waon -i table/'+ident+'/'+target+'-sample.wav -o table/'+ident+'/'+target+'-key.mid');
	debug('Analyzing MIDI keys');
	childGK = exec('./engine/getKey.py table/'+ident+'/'+target+'-key.mid');
	var keyBuff = childGK.toString().replace(/\)/g,"").replace(/\(/g,"").replace(/\'/g,"");
	var keys = csv.parse(keyBuff);
	var key = [ keys[0][0].trim(), keys[0][1].trim() ];
	debug('Found key of '+key[0]+' '+key[1]);
	cb(null,key);
}

module.exports = getKey;
