var exec = require('child_process').execSync;
var fs = require('fs');
var debug = require('debug')('Stretch');

function taskLength(ident,target,cb) {
	childBP = exec('ffprobe -v error -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 table/'+ident+'/'+target+'.wav');
	var seconds = childBP.toString().trim();
	var lengthTime = seconds;
	var bpm = ( 120 / seconds );
	while (bpm < 80) bpm = bpm * 2;
	if (bpm > 160) bpm = bpm / 2;
	cb(null, {
		lengthTime,
		bpm
	});
}

module.exports = taskLength;
