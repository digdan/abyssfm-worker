var exec = require('child_process').execSync;
var fs = require('fs');
var debug = require('debug')('Pitch');

function pitch(ident,target,cents) { //Stretch original
	debug('Pitching sample '+cents+' cents');
	childPS = exec('sox table/'+ident+'/'+target+'.wav table/'+ident+'/'+target+'.wav pitch '+cents);
	return true;
}

module.exports = pitch;
