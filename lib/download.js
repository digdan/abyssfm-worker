var exec = require('child_process').execSync;
var async = require('async');
var fs = require('fs');
var debug = require('debug')('Download');

/*
 *	Download a youtube sample
 *	Create a section of the wav to analyze
 * 	
 *	Return the ID
 */
function download(url,ident,cb) {
	debug('Downloading audio file');
	var dir = 'table/'+ident;

	
	if (!fs.existsSync( dir )) {
		fs.mkdirSync(dir);
	}

	childYT = exec('youtube-dl -x --audio-format wav -o "table/'+ident+'/original.%(ext)s" '+url);
	cb(null);
}

module.exports = download;
