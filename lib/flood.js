var exec = require('child_process').execSync;
var async = require('async');
var fs = require('fs');
var debug = require('debug')('Songer');
var url = require('url');
var csv = require('csv-string');
var youtubeURL = process.argv[2];

var urlParts = url.parse( youtubeURL, true );
var youtubeId = urlParts.query['v'];
var info = {}; //All song info

function puts(err, stdout, stderr) {
	if (err) console.log('err',err);
	if (stderr) console.log('stderr',stderr);
	if (stdout) console.log('stdout',stdout);
}

function seconds2BPM(seconds) {
	//120 = 2HZ = 1 second
	var bpm = (120 / seconds);
	while ( bpm < 80) bpm = bpm * 2;
	if (bpm > 160) bpm = bpm / 2;
	return bpm;
}

async.series([
	function(cb) { //Download Youtube URL
		debug('Downloading audio file');
		info.originalURL = youtubeURL;
		fs.access('table/'+youtubeId+'.wav',fs.constants.R_OK, function(err) {
			if (err) childYT = exec('youtube-dl -x --audio-format wav -o "table/%(id)s.%(ext)s" '+youtubeURL,puts);
			cb();
		});
	} ,
	function (cb) { //Convert to Midi
		debug('Converting WAV to MIDI');
		fs.access('table/'+youtubeId+'.mid',fs.constants.R_OK, function(err) {
			if (err) childWA = exec('./engine/waon -i table/'+youtubeId+'.wav -o table/'+youtubeId+'.mid',puts);
			cb();
		});
	},
	function (cb) { //Analyze keys
		debug('Analyzing MIDI keys');
		childGK = exec('./engine/getKey.py table/'+youtubeId+'.mid', puts );
		var keyBuff = childGK.toString().replace(/\)/g,"").replace(/\(/g,"").replace(/\'/g,"");
		var keys = csv.parse(keyBuff);
		info.key = [ keys[0][0].trim(), keys[0][1].trim() ];
		info.textKey = info.key.join('');
		debug('Found key of '+info.key[0]+' '+info.key[1]);
		cb();
	},
	function (cb) { //Stretch original
		debug('Stretching sample');
		fs.access('table/'+youtubeId+'-stretched.wav',fs.constants.R_OK, function(err) {
			if (err) childPS = exec('./engine/paul.py -s 8 -w 2 table/'+youtubeId+'.wav table/'+youtubeId+'-stretched.wav',puts);
			cb();
		});
	},
	function (cb) { //Create an octive lower 
		fs.access('table/'+youtubeId+'-stretched-octivedown.wav',fs.constants.R_OK, function(err) {
			if (err) shdown = exec('sox table/'+youtubeId+'-stretched.wav table/'+youtubeId+'-stretched-octivedown.wav pitch -1200',puts); // -1200 cents + 700 cents, or octive down up perfect 5th	
			cb();
		});
	},
	function (cb) {
		fs.access('table/'+youtubeId+'-stretched-octivedown.wav',fs.constants.R_OK, function(err) {
			if (err) shdown = exec('sox table/'+youtubeId+'-stretched.wav table/'+youtubeId+'-stretched-octivedown.wav pitch -1200',puts); // -1200 cents + 700 cents, or octive down up perfect 5th	
		});
	},
	function (cb) {
		fs.access('table/'+youtubeId+'-mixed.wav',fs.constants.R_OK, function(err) {
			if (err) mixdown = exec('sox -m table/'+youtubeId+'-stretched.wav table/'+youtubeId+'-stretched-octivedown.wav table/'+youtubeId+'-mixed.wav',puts);
			cb();
		});
	},
	function (cb) { //Build BPM from original
		childBP = exec('ffprobe -v error -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 table/'+youtubeId+'-mixed.wav',puts);
		var seconds = childBP.toString().trim();
		info.lengthSeconds = seconds;
		info.BPM = seconds2BPM( seconds );
		cb();
	},
	function (cb) { //Convert to MP3
		childLM = exec('lame table/'+youtubeId+'-mixed.wav table/'+youtubeId+'-'+info.textKey+'-'+Math.round(info.BPM)+'.mp3',puts);
		cb();
	},
	function (cb) { //Clean up
		childRM = exec('rm table/'+youtubeId+'-*.wav',puts);
		childRM = exec('rm table/'+youtubeId+'.mid',puts);
		cb();
	},
	function (cb) { //Write JSON file
		fs.writeFile('table/'+youtubeId+'.json', JSON.stringify(info));
		cb();
	}
], function( results, cb) {
	debug('Completed');
	console.log(JSON.stringify(info));	
});


/*
youtube-dl -x -o "out.wav" --audio-format wav $1 
python paulstretch.py -s 8 out.wav
ffmpeg out.wav out.mp3
*/
